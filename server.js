/**
 * Require app dependencies
 * @variable {Instance} the koa Instance
 * @class {Object} the app Index Obeject
*/
require('dotenv').config()
const koa = require('koa')
const app = require('./app')

/**
 * Instantiate the koa app
 * @class {Object} initializes our application
*/
new app(koa)
