// Require Dependencies
require('dotenv').config()
const Email = require('../../../server/services/email')
const assert = require('assert')

// Testing "Email Service"
describe('Test Email Service', () => {
    const email = new Email()
    describe('getOptions()', () => {
        it('it should return application options', async () => {
            const emailOptions = await email.getOptions()
            assert.deepEqual(emailOptions, {})
        })
    })
    describe('callback(successMessage)', () => {
        it('should log a success message', async () => {
            try {
                await email.callback('Just testing the notification service')
                assert(true, false, 'no error was thrown which is good')
            } catch (err) {
                assert.deepEqual(err, {})
            }
        })
    })
    describe('sendEmail(config)', () => {
        it('should send an email to me', async () => {
            try {
                const config = {
                    template: 'tem_rPMJvG9KVwF4XQSwxcgkx3x7',
                    template_data:  { first_name: 'Jesse', email_message: 'Just testing the email notification service' },
                    recipient: 'jesseokeya@gmail.com',
                    sender: 'pocketloan@test.com',
                    successMessage: 'Just testing the email notification service'
                }
                await email.sendEmail(config)
                assert(true, false, 'test didnt fail which is good')
            } catch (err) {
                throw err
            }
        })
    })
})
