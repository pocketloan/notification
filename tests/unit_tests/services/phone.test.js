// Require dependencies
require('dotenv').config()
const Phone = require('../../../server/services/phone')
const assert = require('assert')

// Testing "Phone Service"
describe('Test Phone Service', () => {
    const phone = new Phone()
    describe('getOptions()', () => {
        it('it should return phone service options', async () => {
            const phoneOptions = await phone.getOptions()
            assert.deepEqual(phoneOptions, {})
        })
    })
    describe('sendText(config)', () => {
        it('should send a text message to  a user', async () => {
            const config = {
                body: 'Just testing the notification service',
                to: '6134135540'
            }
            try {
                await phone.sendText(config)
                assert(true, false)
            } catch(err) {
                throw err
            }
        })
    })
})
