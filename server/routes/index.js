// Require Dependencies
const NotifyRouter = require('./notify')
const HealthRouter = require('./health')
const Email = require('../services/email')
const Phone = require('../services/phone')
const Token = require('../services/token')
const Slack = require('../services/slack')

// Exports and Initializes the application routes
module.exports = (app) => {
  const handlers = [
    new NotifyRouter({ Email, Phone, Token, Slack }),
    new HealthRouter()
  ]

  handlers.forEach(handler => {
    handler.init(app)
  })
}
