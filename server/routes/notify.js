// Require dependencies
const _ = require('lodash')
const Router = require('koa-router')
const { version, sender, slackConfig } = require.main.require('./config')
const { ERROR_SENDING_TEXT, ERROR_SENDING_EMAIL, ERROR_GENERATING_TOKEN, SLACK_ERROR } = require('../errorMessages')

/** @class {Object} Notify
 * @constructor {Object} {Email, Phone, Token}
 * performes user operation POST, GET
 */
class Notify extends Router {
  constructor({ Email, Phone, Token, Slack } = {}) {
    super()
    this.email = new Email()
    this.phone = new Phone()
    this.token = new Token()
    this.slack = new Slack(slackConfig)
  }
  // Initializes The Router
  init(app) {
    this.prefix(`${version}`)
    this.post('/send-text', ctx => this.sendText(ctx))
    this.post('/send-email', ctx => this.sendEmail(ctx))
    this.post('/slackbot', ctx => this.slackBot(ctx))
    this.get('/token', ctx => this.generateToken(ctx))
    this.post('/token', ctx => this.generateToken(ctx))

    app.use(this.routes())
  }

  /**
   * @param {ctx} ctx route context object
   * post route to send text to the user
   */
  async sendText(ctx) {
    try {
      const { body, to } = ctx.request.body
      await this.phone.sendText({ body, to })
      ctx.body = {
        message: 'text message successfully sent',
        status: '200'
      }
    } catch (err) {
      ctx.body = ERROR_SENDING_TEXT
      throw err
    }
  }

  /**
   * @param {ctx} ctx route context object
   * post route to send email to the user
   */
  async sendEmail(ctx) {
    try {
      const { template, recipientAddress, first_name, email_message, user_id } = ctx.request.body
      const successMessage = 'email successfully sent'
      const recipient = { address: recipientAddress }
      const template_data = { first_name, email_message, user_id: !_.isEmpty(user_id) ? user_id : '' }
      await this.email.sendEmail({ template, recipient, sender, template_data, successMessage })
      ctx.body = {
        message: 'email successfully sent',
        status: '200'
      }
    } catch (err) {
      ctx.body = ERROR_SENDING_EMAIL
      throw err
    }
  }

  /**
   * @param {ctx} ctx route context object
   * post route to generate token for user
   */
  async generateToken(ctx) {
    try {
      const validTokenTypes = await this.token.validTokenTypes()
      const query = await this.checkTokenObject(ctx)
      const { length, tokenType } = query
      const tokenLength = _.toNumber(length)
      const isQueryValid = validTokenTypes.includes(tokenType.toLowerCase()) && _.isNumber(tokenLength)
      if (isQueryValid) {
        const token = await this.token.getToken(tokenLength, tokenType)
        if (!_.isEmpty(token)) {
          ctx.body = {
            message: 'token successfully generated',
            status: '200',
            token
          }
        } else {
          ctx.body = {
            message: 'invalid token generator query',
            status: '404'
          }
        }
      } else {
        throw new Error('Invalid query parameters')
      }
    } catch (err) {
      ctx.body = ERROR_GENERATING_TOKEN
      throw err
    }
  }

  async slackBot(ctx) {
    try {
      const { isCustomerServiceMessage } = ctx.query
      if (!_.isEmpty(isCustomerServiceMessage) && isCustomerServiceMessage === 'true') {
        let { message } = ctx.request.body
        message = message.toString()
        const params = {}
        await this.slack.postMessageToChannel('customer_service', message, params)
        ctx.body = {
          status: 200,
          message: `slack message successfully sent`
        }
      } else {
        let { message } = ctx.request.body
        message = message.toString()
        const params = {}
        await this.slack.postMessageToChannel('general', message, params)
        const users = ['olowonyotolulope', 'logo4jesu', 'anuraggagneja8', 'jesseokeya']
        users.forEach(async user => {
          await this.slack.postMessageToUser(user, message, params)
        })
        ctx.body = {
          status: 200,
          message: `slack message successfully sent`
        }
      }
    } catch (err) {
      ctx.status = SLACK_ERROR.status
      ctx.body = SLACK_ERROR
      throw err
    }
  }

  /**
   * @param {ctx} ctx route context object
   * get route to check length and type of token
   */
  async checkTokenObject(ctx) {
    const query = {}
    if (!_.isEmpty(ctx.request.query)) {
      const { length, tokenType } = ctx.request.query
      query.length = length
      query.tokenType = tokenType
    } else {
      const { length, tokenType } = ctx.request.body
      query.length = length
      query.tokenType = tokenType
    }
    return query
  }
}

// Exports the Notify class
module.exports = Notify
