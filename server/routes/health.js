const Router = require('koa-router')
const { version } = require('../../config')
const log = require('custom-logger')
const { UNHEALTHY_APP } = require('../errorMessages')

/**
 @class {Object} HealthRouter
 @constructor {Object} options
 @desc  Checks the health of the application
*/
class HealthRouter extends Router {
    constructor(options = {}) {
        super()
        this.options = options
    }

    // initializes the router
    init(app) {
        this.get('/health', ctx => this.health(ctx))
        this.get(`${version}/health`, ctx => this.health(ctx))
        app.use(this.routes())
    }

    /**
     @param {ctx} ctx route context object
     @desc  get route to check the health status of the application
    */
    async health(ctx) {
        log.info('application is healthy')
        ctx.body = {
            message: 'application is healthy',
            status: 200
        }
    }
}

// Exports the HealthRouter
module.exports = HealthRouter