// Exports The Error Messages
module.exports = {
    ERROR_SENDING_TEXT: {
        status: 404,
        message: 'error sending text message',
        title: 'ERROR-SENDING-TEXT'
    },
    ERROR_SENDING_EMAIL: {
        status: 404,
        message: 'error sending email',
        title: 'ERROR-SENDING-EMAIL'
    },
    SLACK_ERROR: {
        status: 400,
        message: 'error occured while trying to send message to slack',
        title: 'SLACKBOT ERROR'
    },
    ERROR_GENERATING_TOKEN: {
        status: 404,
        message: 'error generating token',
        title: 'ERROR-GENERATING-TOKEN'
    },
    UNHEALTHY_APP: {
        message: 'application is unhealthy',
        status: 404,
        title: 'unhealthy app'
    }
}
