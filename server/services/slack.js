const Bot = require('slackbots')

class Slack extends Bot {
    constructor(options = {}) {
        super(options)
        this.options = options
    }

    start(callback) {
        callback.bind(this)
        return this.on('start', callback)
    }
}

module.exports = Slack