// Require Dependencies
const _ = require('lodash')
const randomize = require('randomatic')

/**
 * @class {Object} Token
 * @constructor {Object} options
 * generate token for users
*/
class Token {
    constructor(options = {}) {
        this.options = options
    }

    /**
     * @param {String} length
     * @return {String} randomizedtoken
     * generate numeric token of specified length
    */
    async generateNumeric(length) {
        return randomize('0', length)
    }

    /**
     * @param {String} length
     * @return {String} randomizedtoken
     * generate alphanumeric token of specified length
    */
    async generateAlphaNumeric(length) {
        return randomize('Aa0', length)
    }

    /**
     * @param {String} length
     * @return {String} randomizedtoken
     * generate token with specialcharacters of specified length
    */
    async generateSpecialCharacters(length) {
        return randomize('!', length)
    }

    /**
     * @param {String} length
     * @return {String} randomizedtoken
     * generate (*) mixed token of specified length
    */
    async generateAll(length) {
        return randomize('*', length)
    }

    /**
     * @param {String} length
     * @return {String} randomizedtoken
     * generate alphabatic token of specified length
    */
    async generateAlpha(length) {
        return randomize('Aa', length)
    }

    /**
     * @param {String} length
     * @param {String} tokenType
     * @return {Object} result
     * get token according to length and type
    */
    async getToken(length, tokenType) {
        let result = 'Invalid query'
        switch (tokenType.toLowerCase()) {
            case 'alpha':
                result = await this.generateAlpha(length)
                break
            case 'numeric':
                result = await this.generateNumeric(length)
                break
            case 'all':
                result = await this.generateAll(length)
                break
            case 'specialcharacters':
                result = await this.generateSpecialCharacters(length)
                break
            case 'alphanumeric':
                result = await this.generateAlphaNumeric(length)
                break
            default:
                break
        }
        return result
    }

    /**
     * @param {}
     * @return {List} ['numeric', 'alpha', 'alphanumeric', 'all', 'specialcharacters']
     * return all types of token
    */
    async validTokenTypes() {
        return ['numeric', 'alpha', 'alphanumeric', 'all', 'specialcharacters']
    }
}

// Exports the Token class
module.exports = Token
