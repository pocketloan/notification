// Require dependencies
const { sendWithusApiKey, logLevel } = require('../../config')
const emailApi = require('sendwithus')(sendWithusApiKey)
const log = require('custom-logger').config(logLevel)

/** @class {Object} Email
 * @constructor {Object} options
 * this class contains the email notification service
*/
class Email {
    constructor(options = {}) {
        this.options = options
    }

   /** @param {Object} config
     *  @return {Promise}
     * sends an email to a particular user
    */
    async sendEmail(config) {
        const { template, template_data, recipient, sender, successMessage } = config
        return emailApi.send({
            template,
            recipient,
            template_data,
            sender
        }, await this.callback(successMessage))
    }

    /** @param {}
     *  @return {Object} options
     * options or configurations of a class
    */
    async getOptions() {
        return this.options
    }

    /** @param {object} successMessage
     * logs a succes message as info
    */
    async callback(successMessage) {
        log.info(successMessage)
    }

}

// Exports the email class
module.exports = Email
