// Require dependencies
const { accountSid, authToken, from } = require('../../config')
const client = require('twilio')(accountSid, authToken)

/** @class {Object} Phone
 * @constructor {Object} options
 * this class contains the text notification service
*/
class Phone {
  constructor(options = {}) {
    this.options = options
  }

  /** @param {Object} config
   *  @return {Promise}
   * this sends text message to a partcular user
  */
  async sendText(config) {
    const { body, to } = config
    return client.messages
      .create({ body, from, to })
      .then(message => console.log(message.sid))
      .done()
  }

  /** @param {}
   *  @return {object} options
   * options or configurations of a class
  */
  async getOptions() {
    return this.options
  }
}

// Exports the Phone class
module.exports = Phone
